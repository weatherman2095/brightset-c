CC=gcc
CFLAGS=-fstack-protector-strong -D_FORTIFY_SOURCE=2 -pie -fpie -fPIE -fstack-protector-all -O2 -mmitigate-rop -Wall -Wextra -Wconversion -Wsign-conversion
# -march=x86_64
# -fstack-clash-protection

.PHONY: default all clean builddir

defaut: all

builddir:
	mkdir -p build

lenovo: builddir
	$(CC) $(CFLAGS) -DINCREMENT=50 -obuild/brightset src/brightset.c
	sha256sum build/brightset > build/brightset.sha

all: builddir
	$(CC) $(CFLAGS) -obuild/brightset src/brightset.c
	sha256sum build/brightset > build/brightset.sha
clean:
	rm -rf build
