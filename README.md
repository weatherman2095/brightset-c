# Brightset

This is just a helper I made for my laptop to use with i3wm to control
the monitor brightness with a few keybind calls in my i3 config file.

Made in C both because I could and because SUID only reliably works
with binaries, so some language that builds binaries was needed
anyway.

I used ACL to restrict users able to run it, because some program
deciding to change brightness on its own would be annoying, if
ultimately harmless.

The hardcoded increment values mean that integer overflows and
underflows won't happen.
