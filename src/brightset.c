#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#define CURRENT_PATH "/sys/class/backlight/intel_backlight/actual_brightness"
#define MAX_PATH "/sys/class/backlight/intel_backlight/max_brightness"
#define TARGET "/sys/class/backlight/intel_backlight/brightness"
#ifndef INCREMENT
#define INCREMENT 500
#endif

int get_file_val(char * path){
  FILE * f = NULL;
  char * line = NULL;
  size_t len = 0;

  if ((f = fopen(path, "r")) == NULL){
    perror("fopen");
    exit(EXIT_FAILURE);
  }

  if (getline(&line, &len, f) == -1){
    perror("getline");
    free(line);
    exit(EXIT_FAILURE);
  }
  int num = atoi(line);
  free(line);

  if (fclose(f) != 0){
    perror("fclose");
    exit(EXIT_FAILURE);
  }

  return num;
}

int main(int argc, char ** argv){
  int MAX = get_file_val(MAX_PATH);
  int CURRENT = get_file_val(CURRENT_PATH);

  int opt;
  char * val = NULL;
  while ((opt = getopt(argc, argv, "a:")) != -1){
    switch (opt) {
    case 'a': {
      val = optarg;
      break;
    }
    default:
      break;
    }
  }

  if (val == NULL){
    fprintf(stderr, "Failed to get argument.\n");
    exit(1);
  }

  printf("MAX: %d, CURRENT: %d\n", MAX, CURRENT);

  int target_val;
  size_t arglen = strlen(val);
  int used = -1;

  if (strncmp(val, "increase", arglen + 1) == 0){
    used = 0;
    if (CURRENT > (MAX - INCREMENT)){
      target_val = MAX;
    } else {
      target_val = CURRENT + INCREMENT;
    }
  }

  if (strncmp(val, "decrease", arglen + 1) == 0){
    used = 0;
    if (CURRENT < (0 + INCREMENT)){
      target_val = 0;
    } else {
      target_val = CURRENT - INCREMENT;
    }    
  }

  if (used != 0){
    fprintf(stderr,"Unknown argument used.\n");
    exit(1);
  }

  FILE * f;
  if ((f = fopen(TARGET, "wb")) == NULL){
    perror("fopen");
    exit(EXIT_FAILURE);
  }

  if (fprintf(f, "%d\n", target_val) < 3){
    perror("fprintf");
    exit(EXIT_FAILURE);
  }

  if (fclose(f) == EOF){
    perror("fclose");
    exit(EXIT_FAILURE);
  }
  printf("NEW: %d\n", target_val);

  return 0;
}
